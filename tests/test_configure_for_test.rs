#![forbid(unsafe_code)]

use log::{debug, error, info, trace, warn};

#[test]
fn test_reconfigure() {
    logsley::configure_for_test("trace", logsley::OutputFormat::Full).unwrap();
    logsley::configure_for_test("trace", logsley::OutputFormat::Full).unwrap();
    logsley::configure_for_test("info", logsley::OutputFormat::Full).unwrap_err();
    logsley::configure_for_test("trace", logsley::OutputFormat::Compact).unwrap_err();
    logsley::configure_for_test("trace", logsley::OutputFormat::Full).unwrap();
    error!("error");
    warn!("warn");
    info!("info");
    debug!("debug");
    trace!("trace");
}
