// You should do logging like this.

fn main() {
    let _global_logger_guard = logsley::configure("info").unwrap();
    logsley::thread_scope("main", || {
        // Log named values:
        logsley::error!("logsley error {}", 1; "x" => 2);
        logsley::warn!("logsley warn {}", 1; "x" => 2);
        logsley::info!("logsley info {}", 1; "x" => 2);
        logsley::debug!("logsley debug {}", 1; "x" => 2);
        logsley::trace!("logsley trace {}", 1; "x" => 2);

        // Log simple messages:
        log::info!("log {}", 1);

        std::thread::spawn(|| {
            logsley::thread_scope("thread1", || {
                logsley::info!("logsley in thread {}", 1; "x" => 2);
                log::info!("log in thread {}", 1);
            })
        })
        .join()
        .unwrap();

        async_std::task::block_on(logsley::task_scope("task1", async move {
            logsley::info!("logsley in task {}", 1; "x" => 2);
            log::info!("log in task {}", 1);
        }));

        panic!("uhoh");
    });
}

// $ cargo run --package logsley --example opinion
// {"time_ns":1604899904064111000,"time":"2020-11-08T21:31:44.064-08:00","module":"opinion","level":"ERROR","message":"logsley error 1","thread":"main","x":2}
// {"time_ns":1604899904064926000,"time":"2020-11-08T21:31:44.064-08:00","module":"opinion","level":"WARN","message":"logsley warn 1","thread":"main","x":2}
// {"time_ns":1604899904065007000,"time":"2020-11-08T21:31:44.065-08:00","module":"opinion","level":"INFO","message":"logsley info 1","thread":"main","x":2}
// {"time_ns":1604899904065070000,"time":"2020-11-08T21:31:44.065-08:00","module":"opinion","level":"INFO","message":"log 1","thread":"main"}
// {"time_ns":1604899904065111000,"time":"2020-11-08T21:31:44.065-08:00","module":"opinion","level":"INFO","message":"logsley in thread 1","thread":"thread1","x":2}
// {"time_ns":1604899904065166000,"time":"2020-11-08T21:31:44.065-08:00","module":"opinion","level":"INFO","message":"log in thread 1","thread":"thread1"}
// {"time_ns":1604899904065241000,"time":"2020-11-08T21:31:44.065-08:00","module":"opinion","level":"INFO","message":"logsley in task 1","task":"task1","thread":"main","x":2}
// {"time_ns":1604899904065292000,"time":"2020-11-08T21:31:44.065-08:00","module":"opinion","level":"INFO","message":"log in task 1","task":"task1","thread":"main"}
// {"time_ns":1604899904955593000,"time":"2020-11-08T21:31:44.955-08:00","module":"log_panics","level":"ERROR","message":"thread 'main' panicked at 'uhoh': examples/opinion.rs:30\n   0: backtrace::backtrace::libunwind::trace\n             at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/backtrace-0.3.54/src/backtrace/libunwind.rs:90:5\n      backtrace::backtrace::trace_unsynchronized\n             at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/backtrace-0.3.54/src/backtrace/mod.rs:66:5\n   1: backtrace::backtrace::trace\n             at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/backtrace-0.3.54/src/backtrace/mod.rs:53:14\n   2: backtrace::capture::Backtrace::create\n             at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/backtrace-0.3.54/src/capture.rs:176:9\n   3: backtrace::capture::Backtrace::new\n             at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/backtrace-0.3.54/src/capture.rs:140:22\n   4: log_panics::init::{{closure}}\n             at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/log-panics-2.0.0/src/lib.rs:52:25\n   5: std::panicking::rust_panic_with_hook\n             at /rustc/4c78178b1c6ee26e493edb58cefaa77ca44e3991/library/std/src/panicking.rs:581:17\n   6: std::panicking::begin_panic::{{closure}}\n             at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/std/src/panicking.rs:506:9\n   7: std::sys_common::backtrace::__rust_end_short_backtrace\n             at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/std/src/sys_common/backtrace.rs:153:18\n   8: std::panicking::begin_panic\n             at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/std/src/panicking.rs:505:12\n   9: opinion::main::{{closure}}\n             at examples/opinion.rs:30:9\n  10: slog_scope::scope\n             at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/slog-scope-4.3.0/lib.rs:248:5\n  11: logsley::thread_scope\n             at src/lib.rs:179:5\n  12: opinion::main\n             at examples/opinion.rs:5:5\n  13: core::ops::function::FnOnce::call_once\n             at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/core/src/ops/function.rs:227:5\n  14: std::sys_common::backtrace::__rust_begin_short_backtrace\n             at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/std/src/sys_common/backtrace.rs:137:18\n  15: std::rt::lang_start::{{closure}}\n             at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/std/src/rt.rs:66:18\n  16: core::ops::function::impls::<impl core::ops::function::FnOnce<A> for &F>::call_once\n             at /rustc/4c78178b1c6ee26e493edb58cefaa77ca44e3991/library/core/src/ops/function.rs:259:13\n      std::panicking::try::do_call\n             at /rustc/4c78178b1c6ee26e493edb58cefaa77ca44e3991/library/std/src/panicking.rs:381:40\n      std::panicking::try\n             at /rustc/4c78178b1c6ee26e493edb58cefaa77ca44e3991/library/std/src/panicking.rs:345:19\n      std::panic::catch_unwind\n             at /rustc/4c78178b1c6ee26e493edb58cefaa77ca44e3991/library/std/src/panic.rs:382:14\n      std::rt::lang_start_internal\n             at /rustc/4c78178b1c6ee26e493edb58cefaa77ca44e3991/library/std/src/rt.rs:51:25\n  17: std::rt::lang_start\n             at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/std/src/rt.rs:65:5\n  18: _main\n","thread":"main"}

// $ DEV_LOG_FORMAT=plain cargo run --package logsley --example opinion
// 2020-11-08T21:32:10.659-08:00 ERRO logsley error 1, x: 2, thread: main
// 2020-11-08T21:32:10.660-08:00 WARN logsley warn 1, x: 2, thread: main
// 2020-11-08T21:32:10.660-08:00 INFO logsley info 1, x: 2, thread: main
// 2020-11-08T21:32:10.660-08:00 INFO log 1, thread: main
// 2020-11-08T21:32:10.660-08:00 INFO logsley in thread 1, x: 2, thread: thread1
// 2020-11-08T21:32:10.660-08:00 INFO log in thread 1, thread: thread1
// 2020-11-08T21:32:10.660-08:00 INFO logsley in task 1, x: 2, task: task1, thread: main
// 2020-11-08T21:32:10.660-08:00 INFO log in task 1, task: task1, thread: main
// 2020-11-08T21:32:11.557-08:00 ERRO thread 'main' panicked at 'uhoh': examples/opinion.rs:30
//    0: backtrace::backtrace::libunwind::trace
//              at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/backtrace-0.3.54/src/backtrace/libunwind.rs:90:5
//       backtrace::backtrace::trace_unsynchronized
//              at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/backtrace-0.3.54/src/backtrace/mod.rs:66:5
//    1: backtrace::backtrace::trace
//              at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/backtrace-0.3.54/src/backtrace/mod.rs:53:14
//    2: backtrace::capture::Backtrace::create
//              at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/backtrace-0.3.54/src/capture.rs:176:9
//    3: backtrace::capture::Backtrace::new
//              at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/backtrace-0.3.54/src/capture.rs:140:22
//    4: log_panics::init::{{closure}}
//              at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/log-panics-2.0.0/src/lib.rs:52:25
//    5: std::panicking::rust_panic_with_hook
//              at /rustc/4c78178b1c6ee26e493edb58cefaa77ca44e3991/library/std/src/panicking.rs:581:17
//    6: std::panicking::begin_panic::{{closure}}
//              at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/std/src/panicking.rs:506:9
//    7: std::sys_common::backtrace::__rust_end_short_backtrace
//              at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/std/src/sys_common/backtrace.rs:153:18
//    8: std::panicking::begin_panic
//              at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/std/src/panicking.rs:505:12
//    9: opinion::main::{{closure}}
//              at examples/opinion.rs:30:9
//   10: slog_scope::scope
//              at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/slog-scope-4.3.0/lib.rs:248:5
//   11: logsley::thread_scope
//              at src/lib.rs:179:5
//   12: opinion::main
//              at examples/opinion.rs:5:5
//   13: core::ops::function::FnOnce::call_once
//              at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/core/src/ops/function.rs:227:5
//   14: std::sys_common::backtrace::__rust_begin_short_backtrace
//              at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/std/src/sys_common/backtrace.rs:137:18
//   15: std::rt::lang_start::{{closure}}
//              at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/std/src/rt.rs:66:18
//   16: core::ops::function::impls::<impl core::ops::function::FnOnce<A> for &F>::call_once
//              at /rustc/4c78178b1c6ee26e493edb58cefaa77ca44e3991/library/core/src/ops/function.rs:259:13
//       std::panicking::try::do_call
//              at /rustc/4c78178b1c6ee26e493edb58cefaa77ca44e3991/library/std/src/panicking.rs:381:40
//       std::panicking::try
//              at /rustc/4c78178b1c6ee26e493edb58cefaa77ca44e3991/library/std/src/panicking.rs:345:19
//       std::panic::catch_unwind
//              at /rustc/4c78178b1c6ee26e493edb58cefaa77ca44e3991/library/std/src/panic.rs:382:14
//       std::rt::lang_start_internal
//              at /rustc/4c78178b1c6ee26e493edb58cefaa77ca44e3991/library/std/src/rt.rs:51:25
//   17: std::rt::lang_start
//              at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/std/src/rt.rs:65:5
//   18: _main
// , thread: main

// The 'full' format is like 'plain' but shows severity in color.
// $ DEV_LOG_FORMAT=full cargo run --package logsley --example opinion
// 2020-11-08T21:34:04.516-08:00 ERRO logsley error 1, x: 2, thread: main
// 2020-11-08T21:34:04.517-08:00 WARN logsley warn 1, x: 2, thread: main
// 2020-11-08T21:34:04.517-08:00 INFO logsley info 1, x: 2, thread: main
// 2020-11-08T21:34:04.517-08:00 INFO log 1, thread: main
// 2020-11-08T21:34:04.517-08:00 INFO logsley in thread 1, x: 2, thread: thread1
// 2020-11-08T21:34:04.517-08:00 INFO log in thread 1, thread: thread1
// 2020-11-08T21:34:04.517-08:00 INFO logsley in task 1, x: 2, task: task1, thread: main
// 2020-11-08T21:34:04.517-08:00 INFO log in task 1, task: task1, thread: main
// 2020-11-08T21:34:05.411-08:00 ERRO thread 'main' panicked at 'uhoh': examples/opinion.rs:30
//    0: backtrace::backtrace::libunwind::trace
//              at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/backtrace-0.3.54/src/backtrace/libunwind.rs:90:5
//       backtrace::backtrace::trace_unsynchronized
//              at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/backtrace-0.3.54/src/backtrace/mod.rs:66:5
//    1: backtrace::backtrace::trace
//              at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/backtrace-0.3.54/src/backtrace/mod.rs:53:14
//    2: backtrace::capture::Backtrace::create
//              at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/backtrace-0.3.54/src/capture.rs:176:9
//    3: backtrace::capture::Backtrace::new
//              at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/backtrace-0.3.54/src/capture.rs:140:22
//    4: log_panics::init::{{closure}}
//              at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/log-panics-2.0.0/src/lib.rs:52:25
//    5: std::panicking::rust_panic_with_hook
//              at /rustc/4c78178b1c6ee26e493edb58cefaa77ca44e3991/library/std/src/panicking.rs:581:17
//    6: std::panicking::begin_panic::{{closure}}
//              at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/std/src/panicking.rs:506:9
//    7: std::sys_common::backtrace::__rust_end_short_backtrace
//              at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/std/src/sys_common/backtrace.rs:153:18
//    8: std::panicking::begin_panic
//              at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/std/src/panicking.rs:505:12
//    9: opinion::main::{{closure}}
//              at examples/opinion.rs:30:9
//   10: slog_scope::scope
//              at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/slog-scope-4.3.0/lib.rs:248:5
//   11: logsley::thread_scope
//              at src/lib.rs:179:5
//   12: opinion::main
//              at examples/opinion.rs:5:5
//   13: core::ops::function::FnOnce::call_once
//              at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/core/src/ops/function.rs:227:5
//   14: std::sys_common::backtrace::__rust_begin_short_backtrace
//              at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/std/src/sys_common/backtrace.rs:137:18
//   15: std::rt::lang_start::{{closure}}
//              at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/std/src/rt.rs:66:18
//   16: core::ops::function::impls::<impl core::ops::function::FnOnce<A> for &F>::call_once
//              at /rustc/4c78178b1c6ee26e493edb58cefaa77ca44e3991/library/core/src/ops/function.rs:259:13
//       std::panicking::try::do_call
//              at /rustc/4c78178b1c6ee26e493edb58cefaa77ca44e3991/library/std/src/panicking.rs:381:40
//       std::panicking::try
//              at /rustc/4c78178b1c6ee26e493edb58cefaa77ca44e3991/library/std/src/panicking.rs:345:19
//       std::panic::catch_unwind
//              at /rustc/4c78178b1c6ee26e493edb58cefaa77ca44e3991/library/std/src/panic.rs:382:14
//       std::rt::lang_start_internal
//              at /rustc/4c78178b1c6ee26e493edb58cefaa77ca44e3991/library/std/src/rt.rs:51:25
//   17: std::rt::lang_start
//              at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/std/src/rt.rs:65:5
//   18: _main
// , thread: main

// The 'compact' format shows severity in color and prints scope variables on their own line.
// $ DEV_LOG_FORMAT=compact cargo run --package logsley --example opinion
// thread: main
//  2020-11-08T21:32:57.238-08:00 ERRO logsley error 1, x: 2
//  2020-11-08T21:32:57.239-08:00 WARN logsley warn 1, x: 2
//  2020-11-08T21:32:57.239-08:00 INFO logsley info 1, x: 2
//  2020-11-08T21:32:57.239-08:00 INFO log 1
// thread: thread1
//  2020-11-08T21:32:57.239-08:00 INFO logsley in thread 1, x: 2
//  2020-11-08T21:32:57.239-08:00 INFO log in thread 1
// thread: main
//  task: task1
//   2020-11-08T21:32:57.239-08:00 INFO logsley in task 1, x: 2
//   2020-11-08T21:32:57.239-08:00 INFO log in task 1
//  2020-11-08T21:32:58.132-08:00 ERRO thread 'main' panicked at 'uhoh': examples/opinion.rs:30
//    0: backtrace::backtrace::libunwind::trace
//              at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/backtrace-0.3.54/src/backtrace/libunwind.rs:90:5
//       backtrace::backtrace::trace_unsynchronized
//              at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/backtrace-0.3.54/src/backtrace/mod.rs:66:5
//    1: backtrace::backtrace::trace
//              at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/backtrace-0.3.54/src/backtrace/mod.rs:53:14
//    2: backtrace::capture::Backtrace::create
//              at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/backtrace-0.3.54/src/capture.rs:176:9
//    3: backtrace::capture::Backtrace::new
//              at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/backtrace-0.3.54/src/capture.rs:140:22
//    4: log_panics::init::{{closure}}
//              at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/log-panics-2.0.0/src/lib.rs:52:25
//    5: std::panicking::rust_panic_with_hook
//              at /rustc/4c78178b1c6ee26e493edb58cefaa77ca44e3991/library/std/src/panicking.rs:581:17
//    6: std::panicking::begin_panic::{{closure}}
//              at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/std/src/panicking.rs:506:9
//    7: std::sys_common::backtrace::__rust_end_short_backtrace
//              at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/std/src/sys_common/backtrace.rs:153:18
//    8: std::panicking::begin_panic
//              at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/std/src/panicking.rs:505:12
//    9: opinion::main::{{closure}}
//              at examples/opinion.rs:30:9
//   10: slog_scope::scope
//              at /Users/user/.cargo/registry/src/github.com-1ecc6299db9ec823/slog-scope-4.3.0/lib.rs:248:5
//   11: logsley::thread_scope
//              at src/lib.rs:179:5
//   12: opinion::main
//              at examples/opinion.rs:5:5
//   13: core::ops::function::FnOnce::call_once
//              at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/core/src/ops/function.rs:227:5
//   14: std::sys_common::backtrace::__rust_begin_short_backtrace
//              at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/std/src/sys_common/backtrace.rs:137:18
//   15: std::rt::lang_start::{{closure}}
//              at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/std/src/rt.rs:66:18
//   16: core::ops::function::impls::<impl core::ops::function::FnOnce<A> for &F>::call_once
//              at /rustc/4c78178b1c6ee26e493edb58cefaa77ca44e3991/library/core/src/ops/function.rs:259:13
//       std::panicking::try::do_call
//              at /rustc/4c78178b1c6ee26e493edb58cefaa77ca44e3991/library/std/src/panicking.rs:381:40
//       std::panicking::try
//              at /rustc/4c78178b1c6ee26e493edb58cefaa77ca44e3991/library/std/src/panicking.rs:345:19
//       std::panic::catch_unwind
//              at /rustc/4c78178b1c6ee26e493edb58cefaa77ca44e3991/library/std/src/panic.rs:382:14
//       std::rt::lang_start_internal
//              at /rustc/4c78178b1c6ee26e493edb58cefaa77ca44e3991/library/std/src/rt.rs:51:25
//   17: std::rt::lang_start
//              at /Users/user/.rustup/toolchains/beta-x86_64-apple-darwin/lib/rustlib/src/rust/library/std/src/rt.rs:65:5
//   18: _main
