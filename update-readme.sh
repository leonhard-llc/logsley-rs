#!/usr/bin/env bash
set -e
cd "$(dirname "$0")"
echo "PWD=$PWD"

filename=Readme.md
while :; do
  case "$1" in
  --check)
    filename=Readme.md.tmp
    check=1
    ;;
  '') break ;;
  *) usage "bad argument '$1'" ;;
  esac
  shift
done

set -e
set -x
cargo readme --no-title --no-indent-headings >"$filename"
set +x
if [ -n "$check" ]; then
  diff Readme.md Readme.md.tmp || (
    echo "Readme.md is stale" >&2
    exit 1
  )
  rm -f Readme.md.tmp
  git rm -f --ignore-unmatch Readme.md.tmp
fi
