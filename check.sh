#!/usr/bin/env bash
# This script uses bash because it has a built-in 'time' command.
# The rust docker images have no 'time' binary and use dash for 'sh' which has
# no built-in 'time' command.

set -e
cd "$(dirname "$0")"
echo "PWD=$PWD"

check() {
  set -x
  time cargo check --verbose
  ./update-readme.sh --check
  time cargo build --verbose
  time cargo test --features=async --verbose
  time cargo fmt --all -- --check
  time cargo clippy -- -D warnings
  time cargo publish --dry-run "$@"
  set +x
  echo "$0 finished"
}
time check "$@"
